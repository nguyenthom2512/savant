import * as React from 'react';
import { NavigationContainer, NavigationContainerRef } from '@react-navigation/native';
import RootStack from './modules/navigation';
import NavigationService from 'shared/helpers/NavigationService';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { Host } from 'react-native-portalize';

export default function App() {

  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <NavigationContainer
        ref={(ref: NavigationContainerRef<any>) => { NavigationService.setTopLevelNavigator(ref) }}
      >
        <Host>
          <RootStack />
        </Host>
      </NavigationContainer>
    </GestureHandlerRootView>
  );
}
