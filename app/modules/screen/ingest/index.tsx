import React, { useEffect, useState, useRef } from 'react';
import {
    View,
    StyleSheet,
    Button,
    TouchableOpacity,
    Text,
    Linking,
    Image,
} from 'react-native';
import { Camera, useCameraDevice, useCameraDevices } from 'react-native-vision-camera';

const IngestScreen = () => {
    const [showCamera, setShowCamera] = useState(false);
    const [imageSource, setImageSource] = useState('');
    const [showButtonTakePhoto, setShowButtonTakePhoto] = useState(false)

    const devices = useCameraDevice('back');
    const camera = useRef(null);

    useEffect(() => {
        async function getPermission() {
            const newCameraPermission = await Camera.requestCameraPermission();
            console.log(newCameraPermission);
        }
        getPermission();
    }, []);

    const capturePhoto = async () => {
        if (camera.current !== null) {
            const photo = await camera.current.takePhoto({});
            setImageSource(photo.path);
            setShowCamera(false);
            console.log(photo.path);
        }
    };

    if (devices == null) {
        return <Text>Camera not available</Text>;
    }

    return (
        <View style={styles.container}>
            <View style={{ paddingBottom: 50, flex: 1 }}>
                {!showButtonTakePhoto ?
                    <View style={{marginTop: 50}}>
                        <Button
                            title='Chụp ảnh'
                            onPress={() => {
                                setShowCamera(true)
                                setShowButtonTakePhoto(true)
                            }}
                        />
                    </View>
                    :
                    showCamera ? (
                        <>
                            <Camera
                                ref={camera}
                                style={StyleSheet.absoluteFill}
                                device={devices}
                                isActive={showCamera}
                                photo={true}
                            />

                            <View style={styles.buttonContainer}>
                                <TouchableOpacity
                                    style={styles.camButton}
                                    onPress={() => capturePhoto()}
                                />
                            </View>
                        </>
                    ) : (
                        <>
                            {imageSource !== '' ? (
                                <Image
                                    style={styles.image}
                                    source={{
                                        uri: `file://'${imageSource}`,
                                    }}
                                />
                            ) : null}

                            <View style={styles.backButton}>
                                <TouchableOpacity
                                    style={{
                                        backgroundColor: 'rgba(0,0,0,0.2)',
                                        padding: 10,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: 10,
                                        borderWidth: 2,
                                        borderColor: '#fff',
                                    }}
                                    onPress={() => setShowButtonTakePhoto(false)}>
                                    <Text style={{ color: 'white', fontWeight: '500' }}>Back</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.buttonContainer}>
                                <View style={styles.buttons}>
                                    <TouchableOpacity
                                        style={{
                                            backgroundColor: '#fff',
                                            padding: 10,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            borderRadius: 10,
                                            borderWidth: 2,
                                            borderColor: '#77c3ec',
                                        }}
                                        onPress={() => setShowCamera(true)}>
                                        <Text style={{ color: '#77c3ec', fontWeight: '500' }}>
                                            Retake
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{
                                            backgroundColor: '#77c3ec',
                                            padding: 10,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            borderRadius: 10,
                                            borderWidth: 2,
                                            borderColor: 'white',
                                        }}
                                        onPress={() => setShowCamera(true)}>
                                        <Text style={{ color: 'white', fontWeight: '500' }}>
                                            Use Photo
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </>
                    )
                }


            </View>
        </View>
    );
}

export default IngestScreen;
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    button: {
        backgroundColor: 'gray',
    },
    backButton: {
        backgroundColor: 'rgba(0,0,0,0.0)',
        position: 'absolute',
        justifyContent: 'center',
        width: '100%',
        top: 0,
        padding: 20,
    },
    buttonContainer: {
        position: 'absolute',
        bottom: 80,
        left: 0,
        right: 0,
    },
    buttons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 24
    },
    camButton: {
        height: 80,
        width: 80,
        borderRadius: 40,
        //ADD backgroundColor COLOR GREY
        backgroundColor: '#B2BEB5',

        alignSelf: 'center',
        borderWidth: 4,
        borderColor: 'white',
    },
    image: {
        width: '100%',
        height: '100%',
        aspectRatio: 9 / 16,
    },
});