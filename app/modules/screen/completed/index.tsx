import { View } from "react-native"
import React from "react"
import { AppText } from "../component/AppText"

const CompletedScreen = () => {
    return(
        <View>
            <AppText>CompletedScreen</AppText>
        </View>
    )
}

export default CompletedScreen