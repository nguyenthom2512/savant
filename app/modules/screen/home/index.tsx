import { Formik } from "formik"
import React, { useEffect } from "react"
import { SafeAreaView, View } from "react-native"
import { AppInput } from "../component/AppInput"
import * as yup from "yup"
import appleAuth, { AppleButton } from "@invertase/react-native-apple-authentication"

export const HomeScreen = () => {
    async function onAppleButtonPress() {
        // performs login request
        const appleAuthRequestResponse = await appleAuth.performRequest({
            requestedOperation: appleAuth.Operation.LOGIN,
            // Note: it appears putting FULL_NAME first is important, see issue #293
            requestedScopes: [appleAuth.Scope.FULL_NAME, appleAuth.Scope.EMAIL],
        });

        // get current authentication state for user
        // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
        const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);

        // use credentialState response to ensure the user is authenticated
        if (credentialState === appleAuth.State.AUTHORIZED) {
            console.log('========>>>>>>>>>>>>', appleAuthRequestResponse);
            
            // user is authenticated
        }
    }

    useEffect(() => {
        // onCredentialRevoked returns a function that will remove the event listener. useEffect will call this function when the component unmounts
        return appleAuth.onCredentialRevoked(async () => {
          console.warn('If this function executes, User Credentials have been Revoked');
        });
      }, []);
    return (
        <SafeAreaView>
            <Formik
                initialValues={{
                    text: ''
                }}
                onSubmit={() => { }}
            >
                {({ handleChange, values }) => {
                    return (
                        <View>
                            <AppInput
                                label="Text"
                                value={values?.text}
                                onChangeText={handleChange('text')}
                                placeholder="Enter text"
                            />
                            <AppleButton
                                buttonStyle={AppleButton.Style.WHITE}
                                buttonType={AppleButton.Type.SIGN_IN}
                                style={{
                                    width: 160, // You must specify a width
                                    height: 45, // You must specify a height
                                }}
                                onPress={() => onAppleButtonPress()}
                            />
                        </View>
                    )
                }}

            </Formik>
        </SafeAreaView>
    )
}