import { BottomTabBarProps } from '@react-navigation/bottom-tabs';
import * as React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { AppText } from '../AppText';

const MyTabBar = ({ state, descriptors, navigation }: BottomTabBarProps) => {
    const styles = useStyles()
    return (
        <View style={styles.tabWrapper}>
            {state.routes.map((route: any, index: number) => {
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                        ? options.tabBarLabel
                        : options.title !== undefined
                            ? options.title
                            : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name, route.params);
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                        type: 'tabLongPress',
                        target: route.key,
                    });
                };

                return (
                    <TouchableOpacity
                        key={index}
                        accessibilityRole="button"
                        accessibilityState={isFocused ? { selected: true } : {}}
                        accessibilityLabel={options.tabBarAccessibilityLabel}
                        testID={options.tabBarTestID}
                        onPress={onPress}
                        onLongPress={onLongPress}
                        style={{ flex: 1, alignItems: 'center', justifyContent: 'space-between' }}
                    >
                        <AppText style={{ color: isFocused ? '#673ab7' : '#222' }}>
                            {label}
                        </AppText>
                    </TouchableOpacity>
                );
            })}
        </View>
    );
}

export default MyTabBar

const useStyles = () => StyleSheet.create({
    tabWrapper: {
        paddingTop: 15,
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 24,
        backgroundColor: 'white',
        shadowColor: '#083070',
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowRadius: 6,
        shadowOpacity: 0.08,
        elevation: 2.5,
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
    }
})
