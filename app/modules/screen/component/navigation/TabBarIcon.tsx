import React, { useState } from 'react'
import { StyleProp, StyleSheet, View, ViewStyle } from 'react-native'
import { AppText } from '../AppText';

interface IProps {
    icon: React.ReactNode;
    routeName: string;
    displayName?: string;
    isFocused?: boolean;
    containerStyle?: StyleProp<ViewStyle>;
}

export const TabBarIcon = React.memo((props: IProps) => {
    const styles = useStyles();
    const { icon, containerStyle, isFocused, displayName } = props

    const [badge,] = useState(0)

    return <View style={[styles.container, containerStyle]}>
        {icon}
        {!!displayName ? <AppText style={[styles.txtDisplay, { color: isFocused ? 'red' : 'blue' }]}>{displayName}</AppText> : null}
        {badge ? <View style={styles.badge}></View> : null}
    </View>
})

const useStyles = () => StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtDisplay: {
        fontSize: 11,
        textTransform: 'capitalize'
    },
    badge: {
        position: 'absolute',
        right: -6,
        top: -8,
        backgroundColor: 'red',
        borderRadius: 10,
        width: 20,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: '#ffffff'
    },
})
