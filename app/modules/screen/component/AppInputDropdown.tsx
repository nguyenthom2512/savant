import React from "react"
import { AppInput } from "./AppInput"
import { StyleSheet, TouchableOpacity } from "react-native"
import { CaretDown } from "@phosphor-icons/react"

interface IProps {
    handleChange?: () => void
    value: string
    label: string
    onPress?: () => void
    disabled?: boolean
}

export const AppInputDropdown = React.memo((props: IProps) => {
    const { handleChange, value, label, onPress, disabled } = props
    return (
        <TouchableOpacity onPress={onPress} disabled={disabled}>
            <CaretDown size={16} color={'black'} />
            <AppInput label={label} value={value} onChange={handleChange} />
        </TouchableOpacity>
    )
})

const styles = StyleSheet.create({

})