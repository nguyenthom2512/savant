import React from "react";
import { StyleSheet, TextInput, TextInputProps } from "react-native";
import { AppText } from "./AppText";

export interface IAppInput extends TextInputProps {
    value: string
    label?: string
    error?: string
    requireField?: boolean
    helperText?: string
}
export const AppInput = React.memo((props: IAppInput) => {
    const { value, label, error, requireField, helperText } = props
    return (
        <>
            {!!label && <AppText style={styles.label}>
                {label}
                {requireField === true ? (
                    <AppText style={styles.requireFieldStyle}>*</AppText>
                ) : null}{" "}
            </AppText>}
            <TextInput
                {...props}
                value={value}
                style={styles.containerInput}
            />
            {error && <AppText style={styles.error}>{error}</AppText>}
            {helperText && <AppText style={styles.helperText}>{helperText}</AppText>}
        </>
    )
})

const styles = StyleSheet.create({
    containerInput: {
        borderWidth: 1,
        borderColor: 'gray',
        padding: 20,
        marginTop: 12
    },
    label: {
        color: 'black',
        fontSize: 12,
        lineHeight: 24,
        fontFamily: ''
    },
    error: {
        color: 'red',
        fontSize: 12,
        lineHeight: 24
    },
    requireFieldStyle: {
        color: 'red',
        fontSize: 10,
        lineHeight: 16
    },
    helperText:{
        fontSize: 11,
        color: 'black',
        lineHeight: 20
    }

})