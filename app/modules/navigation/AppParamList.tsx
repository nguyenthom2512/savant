
export type AppTabParamList = {
  IngestTab: undefined;
  CompletedTab: undefined;
}

export type AppStackParamList = {
  AppTab: undefined;
  HomeScreen: undefined
  SplashScreen: undefined;
} & AppTabParamList


export type IngestStackParamList = {
  IngestScreen: undefined;
} & AppStackParamList & AppTabParamList;

export type CompletedStackParamList = {
  CompletedScreen: undefined;
} & AppStackParamList & AppTabParamList;

export type HomeStackParamList = {
  HomeScreen: undefined;
} & AppStackParamList & AppTabParamList;


export type AllRouteParamList = AppStackParamList