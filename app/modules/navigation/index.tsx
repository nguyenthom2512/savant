import { StackActions } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import NavigationService from "shared/helpers/NavigationService";
import AppStack from "./appStack";
import SplashScreen from "./splash";

const Stack = createStackNavigator();

interface IProps { }

export const moveToTopAuthStack = () => {
    NavigationService.topLevelNavigator?.dispatch(StackActions.push("Auth"));
};

export const backToTopAuthStack = () => {
    NavigationService.topLevelNavigator?.dispatch(StackActions.replace("Auth"));
};

export const backToTopAppStack = () => {
    NavigationService.topLevelNavigator?.dispatch(StackActions.replace("App"));
};

const RootStack = (props: IProps) => {
    return (
        <Stack.Navigator
            initialRouteName={'App'}
            screenOptions={{
                presentation: 'card',
                headerShown: false,
                animationTypeForReplace: 'push',
            }}
        >
            <Stack.Screen name={'App'} component={AppStack} options={{ headerShown: false }} />
        </Stack.Navigator>
    );
};

export default RootStack;
