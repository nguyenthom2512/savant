import { StackNavigationProp, createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { AppStackParamList } from "../AppParamList";
import { HomeScreen } from "app/modules/screen/home";
import { EventArg, RouteProp } from "@react-navigation/native";
import IngestScreen from "app/modules/screen/ingest";
import AppTab from "./AppTab";

const Stack = createStackNavigator<AppStackParamList>();

const AppStack = () => {
    return <Stack.Navigator initialRouteName={'AppTab'}
        screenListeners={({ navigation, route }: { navigation: StackNavigationProp<AppStackParamList, 'AppTab', undefined>, route: RouteProp<AppStackParamList, keyof AppStackParamList> }) => ({
            state: (data: EventArg<'state', any, any>) => {
                // any time change screen
            },
        })}
    >
        <Stack.Screen name='AppTab' component={AppTab} options={{ headerShown: false }} />
        <Stack.Screen name='IngestTab' component={IngestScreen} options={{ headerShown: false }} />
        <Stack.Screen name='CompletedTab' component={IngestScreen} options={{ headerShown: false }} />
    </Stack.Navigator>
}

export default AppStack

