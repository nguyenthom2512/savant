import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import CompletedScreen from 'app/modules/screen/completed'
import MyTabBar from 'app/modules/screen/component/navigation/MyTabBar'
import IngestScreen from 'app/modules/screen/ingest'
import React from 'react'
import { Image } from 'react-native'
import { AppTabParamList, CompletedStackParamList, IngestStackParamList } from '../AppParamList'

const Tab = createBottomTabNavigator<AppTabParamList>()
const Ingest = createStackNavigator<IngestStackParamList>()
const Completed = createStackNavigator<CompletedStackParamList>()

interface IProps {

}

const IngestStack = (props: IProps) => {
    return <Ingest.Navigator screenOptions={{}} initialRouteName={'IngestScreen'}>
        <Ingest.Screen name='IngestScreen' component={IngestScreen} options={{ headerTitle: 'Ingest' }} />
    </Ingest.Navigator>
}

const CompletedStack = (props: IProps) => {
    return <Completed.Navigator screenOptions={{}} initialRouteName={'CompletedScreen'}>
        <Completed.Screen name='CompletedScreen' component={CompletedScreen} options={{ headerTitle: 'Completed' }} />
    </Completed.Navigator>
}


const AppTab = (props: IProps) => {
    return <Tab.Navigator
        initialRouteName={'IngestTab'}
        screenOptions={{
            tabBarShowLabel: false,
            headerShown: false,
            tabBarAllowFontScaling: false,
        }}
        tabBar={props => <MyTabBar {...props} />}
    >
        <Tab.Screen
            name='IngestTab'
            component={IngestStack}
            options={{
                tabBarLabel: 'Ingest tab',
            }}
        />
        <Tab.Screen
            name='CompletedTab'
            component={CompletedStack}
            options={{
                tabBarLabel: 'Completed tab',
            }}
        />
    </Tab.Navigator>
}

export default AppTab
