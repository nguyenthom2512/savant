import { Image, StyleSheet, View } from "react-native";
import React, { useLayoutEffect, useRef } from "react";

const SplashScreen = () => {
    // !State
    const didMount = useRef<boolean>(false);

    useLayoutEffect(() => {
        if (!didMount.current) {
            didMount.current = true;
        }
    }, []);

    return (
        <View style={styles.container}>
            <View
                style={{
                    position: "absolute",
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    justifyContent: "center",
                    alignItems: "center",
                }}
            >
                <View style={styles.rowLoading}>
                    <Image
                        source={{
                            uri: 'https://d1hjkbq40fs2x4.cloudfront.net/2017-08-21/files/landscape-photography_1645.jpg'
                        }}
                        style={{ width: 100, height: 100 }} />
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    rowLoading: {
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center",
    },
    imgGif: {
        marginLeft: 6,
        width: 11,
        height: 11,
    },
});

export default SplashScreen;
